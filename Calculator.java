public class Calculator {
	public int add(int x, int y) {
		int z = x + y;
		return z;
	}
	
	public int subtract(int x, int y) {
		int z = x - y;
		return z;
	}
	
	public static int multiply(int x, int y) {
		int z = x * y;
		return z;
	}
	
	public static int divide(int x, int y) {
		int z = x / y;
		return z;
	}

}