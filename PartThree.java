import java.util.Scanner;
public class PartThree {
	public static void main(String[] args){
	 Scanner reader = new Scanner(System.in);
	 System.out.println("Enter value 1");
	 int value1 = reader.nextInt();
	 System.out.println("Enter value 2");
	 int value2 = reader.nextInt();
	 
	 Calculator cal = new Calculator();
	 
	 System.out.println("Sum: " + cal.add(value1, value2));
	 System.out.println("Difference: " + cal.subtract(value1, value2));
	 System.out.println("Multiplication: " + Calculator.multiply(value1, value2));
	 System.out.println("Division: " + Calculator.divide(value1, value2));
	}
}