public class MethodsTest {
	public static void main(String[] args) {
		int x = 5;
		// First Number
		System.out.println("#1: " + x);
		methodNoInputNoReturn();
		// Third Number
		System.out.println("#3: " + x);
		methodOneInputNoReturn(x + 10);
		// Fifth Number
		System.out.println("#5: " + x);
		methodTwoInputNoReturn(3, 3.5);
		int z = methodNoInputReturnInt();
		//Seventh number
		System.out.println("#7: " + z);
		double s = sumSquareRoot(9, 5);
		// Eigth number
		System.out.println("#8: " + s);
		
		String s1 = "java";
		String s2 = "programming";
		// Ninth Number
		System.out.println("#9: " + s1.length());
		// Tenth number
		System.out.println("#10: " + s2.length());
		// Eleventh Number 
		System.out.println("#11: " + SecondClass.addOne(50));
		// Twelfth Number
		SecondClass sc = new SecondClass();
		System.out.println("#12: " + sc.addTwo(50));
	}
	public static void methodNoInputNoReturn() {
		int x = 20;
		// Second Number
		System.out.println("#2: " + x);
		System.out.println("Im in a method that takes no input and returns nothing");
	}
	public static void methodOneInputNoReturn(int y) {
		System.out.println("Inside the method one input no return");
		y = y - 5;
		// Fourth Number
		System.out.println("#4: " + y);
	}
	public static void methodTwoInputNoReturn(int x, double y) {
		// Sixth Numbers
		System.out.println("#6: " + x + " " + y);
	}
	public static int methodNoInputReturnInt() {
		return 5;
	}
	
	public static double sumSquareRoot(int x, int y) {
		double z;
		return z = Math.sqrt((x + y));
		
	}
}